package com.bookscirculation.controller;

import com.bookscirculation.model.Book;
import com.bookscirculation.service.BookService;
import com.bookscirculation.service.PersonService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class BooksControllerTest{

    @MockBean
    private BookService bookService;

    @Autowired
    private MockMvc mockMvc;



    @Test
    @WithMockUser(username = "user1", password = "pwd", roles = "USER")
    void getBooksTest() throws Exception {
        //Given
        Book book1 = Book.builder().name("Book1").build();
        Book book2 = Book.builder().name("Book2").build();

        final List<Book> allBooks = Arrays.asList(book1, book2);


        Mockito.when(bookService.getBooksForMain(false, "", false, "")).thenReturn(allBooks);

        //When
        mockMvc.perform(get("/books"))
                .andExpect(status().isOk());
    }
}
