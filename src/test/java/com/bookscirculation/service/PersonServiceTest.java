package com.bookscirculation.service;

import com.bookscirculation.model.Person;
import com.bookscirculation.model.Role;
import com.bookscirculation.repos.PersonRepo;
import com.bookscirculation.service.MailSender;
import com.bookscirculation.service.PersonService;
import org.hamcrest.CoreMatchers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.TestPropertySources;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class PersonServiceTest {
    @Autowired
    private PersonService personService;

    @MockBean
    private PersonRepo personRepo;

    @MockBean
    private MailSender mailSender;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @Test
    public void addPerson() {
        Person person = new Person();
        person.setEmail("maymaewa@mail.ru");
        boolean isUserCreated = personService.addPerson(person);
        assertTrue(isUserCreated);
        assertNotNull(person.getActivationCode());
        assertTrue(CoreMatchers.is(person.getRoles()).matches(Collections.singleton(Role.USER)));

        Mockito.verify(personRepo, Mockito.times(1)).save(person);
        Mockito.verify(mailSender, Mockito.times(1))
                .send(
                        ArgumentMatchers.eq(person.getEmail()),
                        ArgumentMatchers.anyString(),
                        ArgumentMatchers.anyString()
                );
    }


    @Test
    public void addPersonFailTest(){
        Person person = new Person();
        person.setEmail("maymaewa@mail.ru");
        Mockito.doReturn(new Person())
                .when(personRepo)
                .findByEmail("maymaewa@mail.ru");
        boolean isUserCreated = personService.addPerson(person);
        assertFalse(isUserCreated);
        Mockito.verify(personRepo, Mockito.times(0)).save(ArgumentMatchers.any(Person.class));
        Mockito.verify(mailSender, Mockito.times(0))
                .send(
                        ArgumentMatchers.anyString(),
                        ArgumentMatchers.anyString(),
                        ArgumentMatchers.anyString()
                );

    }

    @Test
    public void activateUser(){
        Person person = new Person();
        person.setActivationCode("b");
        Mockito.doReturn(person)
                .when(personRepo)
                .findByActivationCode("activate");
        boolean isUserActivated = personService.activateUser("activate");
        assertTrue(isUserActivated);
        assertNull(person.getActivationCode());

        Mockito.verify(personRepo, Mockito.times(1)).save(person);
    }

    @Test
    public void activateUserFailTest() {
        boolean isUserActivated = personService.activateUser("activate");

        assertFalse(isUserActivated);

        Mockito.verify(personRepo, Mockito.times(0)).save(ArgumentMatchers.any(Person.class));
    }

    @Test
    public void processOAuthPostLogin() {
        boolean isOauth = personService.processOAuthPostLogin("some@gmail.com", "Firstname Lastname");
        assertTrue(isOauth);
        Mockito.verify(personRepo, Mockito.times(1)).save(ArgumentMatchers.any(Person.class));

    }

    @Test
    public void processOAuthPostLoginFailTest() {
        Mockito.doReturn(new Person())
                .when(personRepo)
                .findByEmail("some@gmail.com");
        boolean isOauth = personService.processOAuthPostLogin("some@gmail.com", "Firstname Lastname");
        assertFalse(isOauth);
        Mockito.verify(personRepo, Mockito.times(0)).save(ArgumentMatchers.any(Person.class));

    }

}