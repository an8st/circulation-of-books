package com.bookscirculation.service;

import com.bookscirculation.model.*;
import com.bookscirculation.repos.DealRepo;
import com.bookscirculation.repos.PersonRepo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
@ExtendWith(SpringExtension.class)
class DealServiceTest {
    @Autowired
    DealService dealService;

    @MockBean
    PersonService personService;

    @MockBean
    PersonRepo personRepo;

    @MockBean
    DealRepo dealRepo;

    @MockBean
    MailSender mailSender;

    @Test
    void createDeal() {
        Book book1 = new Book();
        book1.setReturnDate("");
        Person person1 = Person.builder().email("some1@mail.ru").notice(true).build();
        book1.setUser(person1);
        Book book2 = new Book();
        book2.setReturnDate("");
        Person person2 = Person.builder().email("some2@mail.ru").notice(true).build();
        book2.setUser(person2);

        BookRequest bookRequest = BookRequest.builder().bookDelivery(false)
                .respondentsBook(book2).personInitiator(person1).build();
        ResponseToRequest response = ResponseToRequest.builder().bookDelivery(false).build();
        Deal deal = dealService.createDeal(book1, bookRequest, response);
        assertNotNull(deal);
        assertNotNull(deal.getInitiatorsBook());
        assertFalse(deal.isDelivery());
        assertEquals("wait", deal.getStatus());

        Mockito.verify(dealRepo, Mockito.times(1)).save(deal);
        Mockito.verify(mailSender, Mockito.times(2))
                .send(
                        ArgumentMatchers.anyString(),
                        ArgumentMatchers.anyString(),
                        ArgumentMatchers.anyString()
                );
    }
}