package com.bookscirculation.service;


import com.bookscirculation.model.Book;
import com.bookscirculation.model.Deal;
import com.bookscirculation.model.Person;
import com.bookscirculation.repos.BookRepo;
import com.bookscirculation.repos.DealRepo;
import com.bookscirculation.repos.PersonRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@Slf4j
public class SchedulerService {
    @Autowired
    PersonService personService;

    @Autowired
    PersonRepo personRepo;

    @Autowired
    DealService dealService;

    @Autowired
    DealRepo dealRepo;

    @Autowired
    MailSender mailSender;

    @Autowired
    BookRepo bookRepo;


    @Scheduled(cron = "0 0 0 * * ?")
    public void prolongBookReturnDate() {
        List<Book> books = bookRepo.findAllByReturnDateAndAvailable(LocalDate.now().toString(), true);
        for (Book book : books) {
            LocalDate returnDate = LocalDate.parse(book.getReturnDate());
            returnDate = returnDate.plusDays(14);
            book.setReturnDate(returnDate.toString());
            bookRepo.save(book);
        }
    }

    @Scheduled(cron = "0 0 12 * * ?")
    public void closeDealOnReturnDay() {
        List<Deal> deals = dealRepo.findByStatus("active");
        if (deals.isEmpty())
            return;
        for (Deal deal : deals) {
            log.info(deal.getReturnDate().toString());
            if (deal.getReturnDate().toString().equals(LocalDate.now().toString())) {

                Book initiatorsBook = deal.getInitiatorsBook();
                Book respondentsBook = deal.getRespondentsBook();

                deal.setStatus("completed");
                dealRepo.save(deal);

                initiatorsBook.setAvailable(true);
                bookRepo.save(respondentsBook);
                respondentsBook.setAvailable(true);
                bookRepo.save(initiatorsBook);

                Person initiator = initiatorsBook.getUser();
                Person respondent = respondentsBook.getUser();

                String message = "Deadline for returning a book in the exchange of " +
                        initiatorsBook.getName() + " for " +
                        respondentsBook.getName() + "! The deal is closed now.";
                try {
                    if (initiator.isNotice()) {
                        mailSender.send(initiator.getEmail(), "Return book deadline", message);
                    }
                    if (respondent.isNotice()) {
                        mailSender.send(respondent.getEmail(), "Return book deadline", message);
                    }
                    log.debug("Deal closed. Both letters were send to emails.");
                } catch (Exception e) {
                    log.error("Email can't be sent. Error: {}", e.getMessage());
                    log.error("Email can't be sent", e);
                }
            }

        }
    }

}
