package com.bookscirculation.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/ws").withSockJS();
        // с этим будет связываться клиент
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        // что сообщения, место назначения которых начинается с «/ app»,
        // должны направляться в методы обработки сообщений
        registry.setApplicationDestinationPrefixes("/app");
        //   Брокер сообщений рассылает сообщения всем подключенным клиентам, которые подписаны на определенную тему.
        registry.enableSimpleBroker("/channel");
    }


}
