[comment]: <> (# Book Circulation 📚)
<img src="https://sun9-84.userapi.com/impg/oc-W9dWObDvhR0sAIFBVGB1nysEwAmWNPMOQVA/_eHnkfmbEGc.jpg?size=2112x374&quality=96&sign=d36f7a45ac48860c596210b601c060a9&type=album" height="100">


Book Circulation is a platform for mutual exchange of books for a certain period or forever, written as a project for the educational program of Netcracker Technology.



# How to run 🪧
Follow the next link and register or login via Google Mail.
https://bookscirculation.herokuapp.com/

# How to use 💻
❇️ First register or sign in.
<br>
❇️ Then add your books at "Add new book" page.

<img alt="Add new book" src="https://sun9-79.userapi.com/impg/UZ-gRM0OH6G8oTlx3HAMOwxwJTzA2--zC1cErQ/HDV2JcdZ7Ts.jpg?size=1280x650&quality=96&sign=410bb8aae019989603121a3f6c6b7feb&type=album" height="350">
<br>
<br>
❇️ You may find, edit or delete your books at "My books" page.
<br>
<img alt="My books" src="https://sun9-85.userapi.com/impg/PvH7bhyw2tLQhYrvzQ4fU5NBn9j6D_sz0lL9yA/dzxNJKx6R0k.jpg?size=1280x687&quality=96&sign=136e06a997f82d25341c4b7239fa4bb5&type=album" height="350">
<br>
<br>

❇️ Then select the book you like from the main list and send a request to the owner. You may find outgoing requests and their status at "Requests from me" page.
<br>
<img src="https://sun9-75.userapi.com/impg/CTqbsY2uODbTw9pNcu_KXz-G2q6_AxbklCpniA/PoPxf1dlZrw.jpg?size=2560x1449&quality=96&sign=8916f028e1d2db6f49ad7aa23f8dd089&type=album" height="370">
<br>
<br>
❇️ If user agreed to change and chose one of you books too, you may accept or reject the exchange (at "Possible exchanges" page).
<img src="https://sun9-27.userapi.com/impg/FF79QiofJP4ND25LiW51ioyd73tw87dP4HuAMg/YA3yMGsKSP0.jpg?size=2560x1219&quality=96&sign=f0fac80edd1af7ad4b50c0f7c611fadf&type=album" height="330">
<br>
<br>
❇️ If you agree with the details of exchange, the deal will start and you may find it at "Current deals". Having some questions, you may ask your colleague using chat.
<br>
<img alt="Current deals" src="https://sun9-6.userapi.com/impg/xOEy5IN2QQALd0t_bjOsPMRGmyaOZHw8iUMzNg/5CBW1y65MHo.jpg?size=2528x1292&quality=96&sign=49daa70e828b6031a082e80ee6683587&type=album" height="350">
<img alt="Chat" src="https://sun9-72.userapi.com/impg/0PQgM-4YxgbgmAjSIGD1DD_xaLXm5xC-BTxcgg/nXpayud3aH4.jpg?size=1892x1322&quality=96&sign=1e7764b976a607e85cbe52aa958105cf&type=album" height="360">
<br>
<br>
❇️ The exchange is completed by confirming the two parties and sending a review. If you do not have time to return the user's book in time, send him a request for an extension and the deadline for delivery will be extended.
<br>
❇️ You may find incoming requests at "Requests to me" page (select one of user's books or reject request).
<br>
❇️ You may find reviews on you and edit your personal information by going to "My profile - Edit".
<br>
<img alt="Profile" src="https://sun9-73.userapi.com/impg/UNydJ4yazZWETdCUyuJH3LbMWZB00-9g2t3bkw/daek_J7P6TA.jpg?size=2542x1274&quality=96&sign=8456ae2dcfad44be1f6d1404af57cca6&type=album" height="350">
<br>
# Built with 🛠
Base structure:
<br>
✔️ Spring Boot
<br>
✔️ Maven
<br>
Frontend:
<br>
✔️ Thymeleaf
<br>
✔️ JavaScript
<br>
✔️ JQuery & JQuery UI
<br>
Testing:
<br>
✔️ JUnit5
<br>
Database:
<br>
✔️ PostgreSQL
<br>
✔️ Spring Data JPA (Hibernate)
<br>

# ToDo list 🗓️

🔘 integration with the delivery service
<br>
🔘 payment system
<br>
🔘 book sales
<br>
🔘 collateral system
<br>
🔘 improve the design of the site
<br>
🔘 moderation

# Authors 👩‍💻
[Moskalenko Elizaveta](https://gitlab.com/moskalenko9381)
<br>
[Maymaewa Anastasia](https://gitlab.com/maymaewa)
