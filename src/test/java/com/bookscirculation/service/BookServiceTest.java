package com.bookscirculation.service;

import com.bookscirculation.model.Book;
import com.bookscirculation.repos.BookRepo;
import com.bookscirculation.repos.DealRepo;
import com.bookscirculation.repos.BookRequestRepo;
import com.bookscirculation.repos.ResponseRepo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class BookServiceTest {
    @Autowired
    BookService bookService;

    @MockBean
    BookRepo bookRepo;

    @MockBean
    BookRequestRepo bookRequestRepo;

    @MockBean
    ResponseRepo responseRepo;

    @MockBean
    DealRepo dealRepo;

    @Test
    void deleteBook() {
        Book book = new Book();
        Mockito.doReturn(Optional.of(book))
                .when(bookRepo)
                .findById(1);
        bookService.deleteBook(1);
        Mockito.verify(bookRepo, Mockito.times(1)).deleteById(1);
    }
}