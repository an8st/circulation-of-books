package com.bookscirculation.repos;

import com.bookscirculation.model.Deal;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DealRepo extends CrudRepository<Deal, Integer> {
    @Query("select c from Deal c where c.initiatorsBook.user.id = :id and c.status LIKE %:status% order by c.id desc ")
    List<Deal> findByInitiatorAndStatus(@Param("id") Integer id, @Param("status") String status);

    @Query("select c from Deal c where (c.respondentsBook.user.id = :id or c.initiatorsBook.user.id = :id) and c.status LIKE %:status% order by c.id desc")
    List<Deal> findByInitiatorOrRespondentAndStatus(@Param("id") Integer id, @Param("status") String status);

    @Query("select c from Deal c where (c.respondentsBook.id = :id or c.initiatorsBook.id = :id) and c.status LIKE %:status%")
    List<Deal> findByBookIdAndStatus(@Param("id") Integer id, @Param("status") String status);

    @Query("select c from Deal c where (c.respondentsBook.id = :id or c.initiatorsBook.id = :id)")
    List<Deal> findByBookId(@Param("id") Integer id);


    List<Deal> findByStatus(String status);

}