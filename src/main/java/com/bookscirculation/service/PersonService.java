package com.bookscirculation.service;

import com.bookscirculation.model.*;
import com.bookscirculation.repos.PersonRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import java.util.Collections;
import java.util.UUID;

@Service
@Slf4j
public class PersonService {

    @Autowired
    private PersonRepo personRepo;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private MailSender mailSender;

    @Value("${url}")
    private String url;

    public boolean processOAuthPostLogin(String email, String name) {
        Person existUser = personRepo.findByEmail(email);

        if (existUser == null) {
            String[] names = name.split("\\s");
            Person newUser = Person.builder().email(email).firstname(names[0]).lastname(names[1])
                    .password(passwordEncoder.encode(email)).active(true).notice(true)
                    .provider(Provider.GOOGLE).roles(Collections.singleton(Role.USER)).
                    delivery(false).build();

            personRepo.save(newUser);
            return true;
        }
        return false;

    }

    public boolean addPerson(Person person) {
        log.info("Person to add: " + person.toString());
        Person personFromDb = personRepo.findByEmail(person.getEmail());
        if (personFromDb != null) {
            return false;
        }

        person.setPassword(passwordEncoder.encode(person.getPassword()));
        person.setActive(false);
        person.setRoles(Collections.singleton(Role.USER));
        person.setProvider(Provider.LOCAL);
        person.setDelivery(false);
        person.setNotice(true);
        person.setActivationCode(UUID.randomUUID().toString());
        personRepo.save(person);

        if (!StringUtils.isEmpty(person.getEmail())) {
            String message =
                    "Hello, " + person.getFirstname() + "\nWelcome to Books Circulation. Please, visit next link: " +
                            url + "activate/" + "person.getActivationCode()";
            mailSender.send(person.getEmail(), "Activation code", message);

        }
        return true;
    }

    public boolean activateUser(String code) {
        Person person = personRepo.findByActivationCode(code);

        if (person == null) {
            return false;
        }
        person.setActivationCode(null);
        person.setActive(true);

        personRepo.save(person);
        return true;
    }

    public String currentEmail() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        } else {
            username = ((CustomOAuth2User) principal).getEmail();
        }
        return username;
    }

    public Integer getCurrentId() {
        String username = currentEmail();
        return personRepo.findByEmail(username).getId();
    }

    public void sendReview(int id1, int id2, Review review) {
        int curId = getCurrentId();
        if (curId == id1) {
            Person person1 = personRepo.findById(id1).get();
            review.setWriter(person1);
            Person person2 = personRepo.findById(id2).get();
            review.setRecipient(person2);
        } else if (curId == id2) {
            Person person1 = personRepo.findById(id1).get();
            Person person2 = personRepo.findById(id2).get();
            review.setRecipient(person1);
            review.setWriter(person2);
        }
    }

    public void changePasswordMail(Person person) {
        String message =
                "Hello, " +  person.getFirstname() +
        "\nPlease, visit the next link to change your password at Books Circulation : " + url + "change_password";
        mailSender.send(person.getEmail(), "Change password", message);
    }
}
