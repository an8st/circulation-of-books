package com.bookscirculation.service;

import com.bookscirculation.model.Book;
import com.bookscirculation.model.BookRequest;
import com.bookscirculation.model.Deal;
import com.bookscirculation.model.ResponseToRequest;
import com.bookscirculation.repos.BookRepo;
import com.bookscirculation.repos.BookRequestRepo;
import com.bookscirculation.repos.DealRepo;
import com.bookscirculation.repos.ResponseRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

@Service
public class BookService {
    @Autowired
    BookRepo bookRepo;

    @Autowired
    BookRequestRepo bookRequestRepo;

    @Autowired
    ResponseRepo responseRepo;

    @Autowired
    PersonService personService;

    @Autowired
    DealRepo dealRepo;

    public List<Book> getBooksForMain(boolean checkbox, String name, boolean delivery, String city) {
        List<Book> allBooks;
        Integer userId = personService.getCurrentId();
        if (!checkbox) {
            if (name == null)
                allBooks = bookRepo.findByOrderByIdDescWhereUserIsNotCurrent(userId);
            else
                allBooks = bookRepo.findByNameOrderByIdDescWhereUserIsNotCurrent(name, userId);
        } else {
            if (name == null)
                allBooks = bookRepo.findByAvailableOrderByIdDescWhereUserIsNotCurrent(true, userId);
            else
                allBooks = bookRepo.findByNameAndAvailableOrderByIdDescWhereUserIsNotCurrent(name, true, userId);
        }
        if (delivery) allBooks.removeIf(book -> !book.getUser().isDelivery());
        if (!city.equals("")) allBooks.removeIf(book -> !book.getUser().getCity().equals(city));

        return allBooks;
    }

    public void deleteBook(Integer id) {
        List<BookRequest> requests = bookRequestRepo.findByRespondentsBookId(id);
        for (BookRequest bookRequest : requests) {
            ResponseToRequest response = responseRepo.findByBookRequest(bookRequest);
            responseRepo.delete(response);
        }
        List<ResponseToRequest> responses = responseRepo.findByInitiatorsBookId(id);
        responseRepo.deleteAll(responses);
        for (ResponseToRequest response : responses) {
            bookRequestRepo.delete(response.getBookRequest());
        }

        bookRequestRepo.deleteAll(requests);
        List<Deal> deals = dealRepo.findByBookId(id);
        dealRepo.deleteAll(deals);
        bookRepo.deleteById(id);
    }

    public void addPhotoBook(MultipartFile multipartFile, Book newBook) {

        String fileName = "";
        if (!multipartFile.isEmpty()) {
            fileName = StringUtils.cleanPath(Objects.requireNonNull(multipartFile.getOriginalFilename()));
            newBook.setPhoto(fileName);
        }

        Book savedBook = bookRepo.save(newBook);

        if (!multipartFile.isEmpty()) {
            String uploadDir = "user-photos/" + savedBook.getId();
            try {
                FileUploadUtil.saveFile(uploadDir, fileName, multipartFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void editPhotoBook(MultipartFile multipartFile, Book book) {
        if (!multipartFile.isEmpty()) {
            if (book.getPhotosImagePath() != null) {
                File file = new File(book.getPhotosImagePath());
                file.delete();
            }
            String fileName = StringUtils.cleanPath(Objects.requireNonNull(multipartFile.getOriginalFilename()));
            book.setPhoto(fileName);
            String uploadDir = "user-photos/" + book.getId();
            try {
                FileUploadUtil.saveFile(uploadDir, fileName, multipartFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

