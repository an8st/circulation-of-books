package com.bookscirculation.controller;

import com.bookscirculation.model.*;
import com.bookscirculation.repos.BookRepo;
import com.bookscirculation.repos.BookRequestRepo;
import com.bookscirculation.repos.PersonRepo;
import com.bookscirculation.repos.ReviewRepo;
import com.bookscirculation.service.ResponseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Slf4j
@Controller
public class ResponseController {

    @Autowired
    private PersonRepo personRepo;

    @Autowired
    private BookRepo bookRepo;

    @Autowired
    private BookRequestRepo bookRequestRepo;

    @Autowired
    ResponseService responseService;

    @Autowired
    private ReviewRepo reviewRepo;

    @GetMapping("/requests_to_me/reject/{id}")
    public String reject(@PathVariable Integer id) {
        responseService.createResponseWithReject(id);
        return "redirect:/requests_to_me";
    }

    @GetMapping("/requests_to_me/books/user/{userId}/{reqId}")
    public String getUserBooks(@PathVariable Integer userId, @PathVariable Integer reqId, Model model) {
        Person user = personRepo.findById(userId).get();
        List<Book> myBooks = bookRepo.findByUserId(userId);
        BookRequest req = bookRequestRepo.findById(reqId).get();
        List<Review> reviews = reviewRepo.findByRecipient(user);
        model.addAttribute("books", myBooks);
        model.addAttribute("user", user);
        model.addAttribute("req", req);
        model.addAttribute("reviews", reviews);
        return "user_books";
    }

    @PostMapping("/requests_to_me/agree/{reqId}/{bookId}")
    public String agreeResponse(@PathVariable Integer reqId, @PathVariable Integer bookId, ResponseToRequest response) {
        BookRequest req = bookRequestRepo.findById(reqId).get();
        boolean reqDelivery = req.isBookDelivery();
        if (reqDelivery)
            response.setBookDelivery(true);

        responseService.createAgreeResponse(reqId, bookId, response);
        responseService.sendMailResponse(req.getPersonInitiator());
        return "redirect:/requests_to_me";
    }

    @GetMapping("/requests_to_me/response/{reqId}/{bookId}")
    public String responseDetails(@PathVariable Integer reqId, @PathVariable Integer bookId, Model model) {
        BookRequest req = bookRequestRepo.findById(reqId).get();
        Book book = bookRepo.findById(bookId).get();
        ResponseToRequest response = new ResponseToRequest();
        model.addAttribute("book", book);
        model.addAttribute("response", response);
        model.addAttribute("req", req);
        return "response_details";
    }
}
