package com.bookscirculation.service;

import com.bookscirculation.model.*;
import com.bookscirculation.repos.DealRepo;
import com.bookscirculation.repos.PersonRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import java.time.LocalDate;
import java.util.List;


@Service
@Slf4j
public class DealService {

    @Autowired
    PersonService personService;

    @Autowired
    PersonRepo personRepo;

    @Autowired
    DealRepo dealRepo;

    @Autowired
    MailSender mailSender;

    @Value("${url}")
    private String url;

    public Deal createDeal(Book book, BookRequest req, ResponseToRequest response) {

        Deal deal = Deal.builder().initiatorsBook(book).respondentsBook(req.getRespondentsBook())
                .status("wait").delivery(response.isBookDelivery()).build();

        if (!book.getReturnDate().isEmpty() && !req.getRespondentsBook().getReturnDate().isEmpty()) {
            LocalDate date1 = LocalDate.parse(book.getReturnDate());
            LocalDate date2 = LocalDate.parse(req.getRespondentsBook().getReturnDate());
            if (date1.isBefore(date2)) deal.setReturnDate(date1);
            else deal.setReturnDate(date2);
        } else {
            if (!book.getReturnDate().isEmpty()) {
                deal.setReturnDate(LocalDate.parse(book.getReturnDate()));
            } else if (!req.getRespondentsBook().getReturnDate().isEmpty()) {
                deal.setReturnDate(LocalDate.parse(req.getRespondentsBook().getReturnDate()));
            }
        }
        log.debug("Created deal: " + deal.toString());
        dealRepo.save(deal);
        sendMailDeal(req.getPersonInitiator());
        sendMailDeal(req.getRespondentsBook().getUser());
        return deal;
    }


    public void sendMailDeal(Person person) {
        if (!StringUtils.isEmpty(person.getEmail()) && person.isNotice()) {
            String message =
                    "Hello, " +   person.getFirstname() +
            "\nYou have a new deal. Look at the deals to me on the next link: " + url + "deals";
            mailSender.send(person.getEmail(), "New deal", message);
        }
    }

    public void closeDeal(Integer id) {
        Deal deal = dealRepo.findById(id).get();
        log.debug("Close deal: " + deal);
        Person curUser = personRepo.findById(personService.getCurrentId()).get();
        log.debug("Current user: " + curUser.getEmail());

        if (deal.getInitiatorsBook().getUser() == curUser) {
            deal.setCloseDealByInitiator(true);
            log.debug("Close deal by one side: " + deal.getInitiatorsBook().getUser().getEmail());
        } else if (deal.getRespondentsBook().getUser() == curUser) {
            deal.setCloseDealByRespondent(true);
            log.debug("Close deal by one side: " + deal.getInitiatorsBook().getUser().getEmail());
        }

        if (deal.isCloseDealByInitiator() && deal.isCloseDealByRespondent()) {
            deal.setStatus("completed");

            if (deal.getReturnDate() != null) {
                deal.getInitiatorsBook().setAvailable(true);
                deal.getRespondentsBook().setAvailable(true);
            }

        }
        dealRepo.save(deal);
    }

}
