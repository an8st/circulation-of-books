function getTodaysDate(){
    date = new Date();
    day = date.getDate();
    month = date.getMonth() + 1;
    year = date.getFullYear();

    if (month < 10) month = "0" + month;
    if (day < 10) day = "0" + day;

    let answer = {
        today: 0,
        finish: 0
    };

    answer.today = year + "-" + month + "-" + day;
    finishYear = date.getFullYear() + 1;
    answer.finish = finishYear  + "-" + month + "-" + day;
    return answer;
}
document.getElementById('returnDate').setAttribute('min', getTodaysDate().today);
document.getElementById('returnDate').setAttribute('max', getTodaysDate().finish);