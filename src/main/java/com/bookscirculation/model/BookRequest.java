package com.bookscirculation.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BookRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String status;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "book2_id", referencedColumnName = "id")
    private Book respondentsBook;

    private boolean bookDelivery;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "initiate_user_id", referencedColumnName = "id")
    private Person personInitiator;

    private String note;

    public boolean isRejected() {
        return (status.equals("rejected"));
    }

    public boolean isWait() {
        return (status.equals("wait"));
    }
}
